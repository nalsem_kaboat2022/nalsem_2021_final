#!/usr/bin/env python3
import time
from math import *
from board import SCL, SDA
from busio import I2C
from adafruit_bno08x import (
    BNO_REPORT_STEP_COUNTER,
    BNO_REPORT_ROTATION_VECTOR,
    BNO_REPORT_GEOMAGNETIC_ROTATION_VECTOR,
)
from adafruit_bno08x.i2c import BNO08X_I2C
import rospy
from motor import * 
from std_msgs.msg import Float32
from std_msgs.msg import Int32
from sensor_msgs.msg import LaserScan  
#from tf2_msgs.msg import TFMessage   

M = Motor()
R = 2.0

#imu
i2c = I2C(SCL, SDA, frequency=800000)
bno = BNO08X_I2C(i2c)
bno.enable_feature(BNO_REPORT_STEP_COUNTER)
bno.enable_feature(BNO_REPORT_ROTATION_VECTOR)
bno.enable_feature(BNO_REPORT_GEOMAGNETIC_ROTATION_VECTOR)

def find_heading(dqw, dqx, dqy, dqz):
    norm = sqrt(dqw * dqw + dqx * dqx + dqy * dqy + dqz * dqz)
    dqw = dqw / norm
    dqx = dqx / norm
    dqy = dqy / norm
    dqz = dqz / norm

    ysqr = dqy * dqy

    t3 = +2.0 * (dqw * dqz + dqx * dqy)
    t4 = +1.0 - 2.0 * (ysqr + dqz * dqz)
    yaw_raw = atan2(t3, t4)
    yaw = yaw_raw * 180.0 / 3.141592
    if yaw > 0:
        yaw = 360 - yaw
    else:
        yaw = abs(yaw)
    return yaw  # heading in 360 clockwis

#quat_i, quat_j, quat_k, quat_real = bno.quaternion
#base_heading = find_heading(quat_real, quat_i, quat_j, quat_k)
##imu


def callback(msg):
    quat_i, quat_j, quat_k, quat_real = bno.quaternion
    heading  = find_heading(quat_real, quat_i, quat_j, quat_k)

##desire_set
    n = 1
    theta_list = []    
    theta_sel = []
    desire_list = []
    a = [0,0]
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
    while n < 720 :
        if msg.range[n] <= R :
            if msg.range[n-1] < R:
                pass
            else :
                a[1] = n-1
                theta = (a[1] - a[0]) / 4
                theta_list.append(theta)
                desire = (a[0] + a[1]) / 8
                desire_list.append(desire)
        else:
            if msg.range[n-1] < R:
                a[0] = n
            else :
                pass
        n = n + 1

    if n == 720 :
        a[1] = 720 
        theta = (a[1] - a[0]) / 4
        theta_list.append(theta)
        desire = (a[0] + a[1]) / 8
        desire_list.append(desire)

        i = 0
        while i < len(desire_list):
            if 1/2*R**2*theta_list[i] < area:
                del desire_list[i]



#        if len(desire_list) == 1 & desire_list[0] == 180 :
                    # just R +?


        j = 0
        error_list=list()
        while j < len(desire_list):
            error_list.append(abs(bearing-(heading-(90-desire_list[j]))))
            j = j + 1
    
        k = 1
        m = 0 
        while k < len(error_list):
            if error_list[m] < error_list[k] :
                m = k 
                k = k + 1
        real_desire = 90 - desire_list[m]
        error = real_desire

##ros_setup
def listener():
    rospy.init_node('gh')
    rospy.Subscriber("/scan", LaserScan, callback, queue_size = 1)
    rospy.spin()
##ros_setup

if __name__=='__main__':
    listener()

