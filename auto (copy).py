#!/usr/bin/env python3
import time
#import smbus
from math import *
import serial
import operator
#from motor_t200 import *
from motor_blueonly import *
import rospy
from board import SCL, SDA
from busio import I2C
from std_msgs.msg import Int32
from sensor_msgs.msg import LaserScan 
from adafruit_bno08x import (
    BNO_REPORT_STEP_COUNTER,
    BNO_REPORT_ROTATION_VECTOR,
    BNO_REPORT_GEOMAGNETIC_ROTATION_VECTOR,
)
from adafruit_bno08x.i2c import BNO08X_I2C
import Jetson.GPIO as GPIO


pin_1 = 29
pin_2 = 31
pin_3 = 37



#GPIO.setmode(GPIO.BOARD)
#GPIO.setup(pin_1, GPIO.OUT, initial=GPIO.HIGH)
#GPIO.setup(pin_2, GPIO.OUT, initial=GPIO.HIGH)
#GPIO.setup(pin_3, GPIO.OUT, initial=GPIO.HIGH)

n_a = 10
n_b = 15

R = 3
area = 20	## <<<minimum  so  + need       #mod

PI = 3.141592

##MOTOR SET
M = Motor()

time.sleep(3)
M.motor_move(0,0)
time.sleep(3)
##GPS_setup
ser = serial.Serial(port = "/dev/ttyACM0", baudrate = 38400, timeout = 0.1)	
datName = 'GNGLL'

def GPSabsorption(data):
	data = data.decode("utf-8")
	gps_data = list()
	
	if data[1:6] == datName:
		spliteddata = data.split(",")
		data2 = float(spliteddata[1]) 
		data3 = float(spliteddata[3]) 
		return data2, data3
	return "data invalid"
##GPS_setup

#imu
i2c = I2C(SCL, SDA, frequency=800000)
bno = BNO08X_I2C(i2c)
bno.enable_feature(BNO_REPORT_STEP_COUNTER)
bno.enable_feature(BNO_REPORT_ROTATION_VECTOR)
bno.enable_feature(BNO_REPORT_GEOMAGNETIC_ROTATION_VECTOR)

def find_heading(dqw, dqx, dqy, dqz):
    norm = sqrt(dqw * dqw + dqx * dqx + dqy * dqy + dqz * dqz)
    dqw = dqw / norm
    dqx = dqx / norm
    dqy = dqy / norm
    dqz = dqz / norm

    ysqr = dqy * dqy

    t3 = +2.0 * (dqw * dqz + dqx * dqy)
    t4 = +1.0 - 2.0 * (ysqr + dqz * dqz)
    yaw_raw = atan2(t3, t4)
    yaw = yaw_raw * 180.0 / 3.141592
    if yaw > 0:
        yaw = 360 - yaw
    else:
        yaw = abs(yaw)
    return yaw  # heading in 360 clockwis

quat_i, quat_j, quat_k, quat_real = bno.quaternion
base_heading = find_heading(quat_real, quat_i, quat_j, quat_k)
print("imu_set")
##imu

##function
def ConvertDecimalDegreesToRadians(deg):
	return (deg * PI / 180)

def ConvertRadiansToDecimalDegrees(rad):
	return (rad * 180 / PI)

def GetBearingBetweenPoints(lat1,lon1,lat2,lon2):
	lat1_rad = ConvertDecimalDegreesToRadians(lat1)
	lat2_rad = ConvertDecimalDegreesToRadians(lat2)
	lon_diff_rad = ConvertDecimalDegreesToRadians(lon2-lon1)
	y = sin(lon_diff_rad) * cos(lat2_rad)
	x = cos(lat1_rad) * sin(lat2_rad) - sin(lat1_rad) * cos(lat2_rad) * cos(lon_diff_rad)
	return (ConvertRadiansToDecimalDegrees(atan2(y,x)) + 360) % 360

def GetDistanceBetweenPoints(lat1,lon1,lat2,lon2):
    theta = lon1 - lon2
    dist = sin(ConvertDecimalDegreesToRadians(lat1)) * sin(ConvertDecimalDegreesToRadians(lat2)) + cos(ConvertDecimalDegreesToRadians(lat1)) * cos(ConvertDecimalDegreesToRadians(lat2)) * cos(ConvertDecimalDegreesToRadians(theta))
    dist = acos(dist)
    dist = ConvertRadiansToDecimalDegrees(dist)
    dist = dist * 60 *1.1515
    dist = dist * 1.609344 * 1000
    return dist
##function

##ros_setup
#def loading():
#    rospy.init_node('iko')
#    rospy.Subscribe("/Heading", listener)
#    rospy.spin()
##ros_setup

error_previous = 0
dist_error_pre = 0
error = 0 
Goal_x = 35.0415
Goal_y = 128.3777412
ka = 0
u = 0

def Callback(msg):
	global u	
	global n_a
	global n_b
	n_a = 10
	##GPS calc
	ka = 0 
	while ka == 0:
		data = ser.readline()
		gps_data = GPSabsorption(data)
#		print("before GPS") 
	
		if gps_data != "data invalid":
			x = gps_data[0] * 0.01                   
			y = gps_data[1] * 0.01
			ka = ka + 1
#			u = u + 1
#			if u % 4 == 2:
#				print("light_change")
#				GPIO.output(pin_1, GPIO.HIGH)
#				GPIO.output(pin_2, GPIO.HIGH)
#				GPIO.output(pin_3, GPIO.HIGH)
#			if u % 4 == 0:
#				print("light_change")
#				GPIO.output(pin_1, GPIO.LOW)
#				GPIO.output(pin_2, GPIO.LOW)
#				GPIO.output(pin_3, GPIO.LOW)
	
		else : 
			ka = 0 

	quat_i, quat_j, quat_k, quat_real = bno.quaternion
	heading = find_heading(quat_real, quat_i, quat_j, quat_k)
	##GPS calc

	##func_setup
	bearing = GetBearingBetweenPoints(x,y,Goal_x,Goal_y)
	theta_list = []
	desire_list = []
	a = [0,0]
	a[0] = 0
	##func_setup

	n = 0 
	while n < 720 :
		if msg.ranges[n] <= R :
			if msg.ranges[n-1] < R:
				pass
			else :
				a[1] = n-1
				theta = (a[1] - a[0]) / 4
				theta_list.append(theta)
				desire = (a[0] + a[1]) / 8
				desire_list.append(abs(180-desire))
		else:
			if msg.ranges[n-1] < R:
				a[0] = n
			else :
	
				pass
		n = n + 1
	if n == 720 :
		if a[0] > a[1] :
			a[1] = 720 
			theta = (a[1] - a[0]) / 4
			theta_list.append(theta)
			desire = (a[0] + a[1]) / 8
			desire_list.append(abs(180-desire))

	i = 0
	while i < len(desire_list) :
		if theta_list[i] < area:
			del desire_list[i]
		i = i+1

	error_list = []
	j = 0
	while j < len(desire_list):
		error_list.append(abs(bearing-(heading-(90-desire_list[j]))))
		j = j + 1
	k = 1
	m = 0 

	while k < len(error_list):
		if error_list[m] > error_list[k] :
			m = k 
		k = k + 1
	if desire_list == []:
		error = 0

	error = 90 - desire_list[m]

	if error > 45 : 
		error = 45 
	if error < -45 :
		error = -45
	print("error")
	print (error)

	print("desire_list")
	print(desire_list)
#	if msg.ranges[360] and msg.ranges != 'inf' < 2 : 
#	if msg.ranges[360] < 1.5 :
#		n_a = 5

	if msg.ranges[360] < 1.5 :
		n_a  =  0
		print(" no corner front danger")
	basic_speed = n_a
	rotate_speed =  n_a + n_b * abs(error/45)

	##motor_output
	if error > 5:
		M.motor_move(rotate_speed,basic_speed)
	elif error < -5:
		M.motor_move(basic_speed,rotate_speed)
	else :
#		if desire_list == [] and msg.ranges[360] < 1.5 and msg.ranges != 'inf' :
#			M.motor_move(-15,15)
#			print("corner rotate")
		M.motor_move(basic_speed ,basic_speed)
	time.sleep(0.5)
	

	##motor_output

#		if error > 5:
#			M.motor_move(error, basic_speed, basic_speed,rotate_speed)
#		elif error < -5:
#			M.motor_move(error, basic_speed, rotate_speed, basic_speed)
#		else :
#			M.motor_move(0,15,15,15)

	del theta_list[:]
	del desire_list[:]
	del a[:]
	del error_list[:]

	print("---------------------")
	print(basic_speed)
	print(rotate_speed)


def listener():
	rospy.init_node('gh')
	rospy.Subscriber("/scan", LaserScan, Callback, queue_size = 1, buff_size=2**24)
	rospy.spin()

if __name__ == '__main__' :
	listener()



